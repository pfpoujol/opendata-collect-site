// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDpwm9fHswcj7EbfyH4j9j9L9c6InWi8YY',
    authDomain: 'opendata-collect-site-dev.firebaseapp.com',
    projectId: 'opendata-collect-site-dev',
    storageBucket: 'opendata-collect-site-dev.appspot.com',
    messagingSenderId: '673411516264',
    appId: '1:673411516264:web:6c31893b504915b3dbb8dd'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
